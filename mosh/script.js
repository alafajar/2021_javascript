// const circle = {
//     radius : 1,
//     location :{
//         x:1,
//         y:1
//     },
//     draw: function () {
//         console.log('draw');
//     }
// }

// circle.draw();

//factory function
function createCircle(radius) {
    return {
        radius,
        draw: function () {
            console.log('hellow')
        }
    }
}
const circle  = createCircle(1);

//Constructor function
function Circle(radius) {
    console.log('this', this)
    this.radius = radius;
    this.draw = function (params) {
        console.log('hello')
    }
    return this; //not needed, cause this will return automatically with new keyword
}

//call constructor function
const another = new Circle(1); 
// keyword new membuat empty object
// jika cnstructor function tidak memakan new, object akan menjadi global

