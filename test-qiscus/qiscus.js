const inventory = {
  inventory1: [
    {
      type: 'Book',
      title: 'Jurrasic Park'
    }, {
      type: 'Car',
      brand: 'Mercedes Benz'
    }, {
      type: 'Smartphone',
      brand: 'Samsung',
      operating_system: 'Android'
    }, {
      type: 'Car',
      brand: 'Ferrari'
    }, {
      type: 'Book',
      title: 'Harry Potter and The Chamber of Secret'
    }
  ],
  inventory2: [
    {
      type: 'Car',
      brand: 'Tesla'
    }, {
      type: 'Smartphone',
      brand: 'Apple',
      operating_system: 'iOS'
    }, {
      type: 'Smartphone',
      brand: 'Xiaomi',
      operating_system: 'Android'
    }, {
      type: 'Book',
      title: 'Learning Data Mining with Python'
    }
  ],
  inventory3: [
    {
      type: 'Kulkas',
      brand: 'nike'
    },
    {
      type: 'Smartphone',
      brand: 'nike'
    }
  ]
}

/* output
1. car
2. smartphone
3. book */


let data = [];
Object.keys(inventory).forEach(function(prop) {
    
    const newArr = inventory[prop]  //
    let   num    = 0
    newArr.map(val => {
        let obj = {
            number: num,
            type  : val.type
        }
        num++
        data.push(obj)
    })
});

const result = Array.from(new Set(data.map(a => a.type)))
// .map(type => {
//    return data.find(a => a.type === type)
// });

console.log('result', result)